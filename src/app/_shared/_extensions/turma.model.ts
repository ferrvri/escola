import { Escola } from './escola.model';

export interface Turma{
    id: number;
    nome: string;
    quantiaAlunos: number;
    escolaId: number;
    escola: Escola;
}