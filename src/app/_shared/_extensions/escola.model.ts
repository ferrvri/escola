export interface Escola {
    id: number;
    nome: string;
    rua: string;
    numero: string;
    cidade: string;
    estado: string;
}