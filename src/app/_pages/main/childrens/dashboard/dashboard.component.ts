import { Component, OnInit } from '@angular/core';
import { MockService } from 'src/app/_services/mock.service';
import { Turma } from 'src/app/_shared/_extensions/turma.model';
import { Escola } from 'src/app/_shared/_extensions/escola.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  escolasNumeros: number = 0
  turmasNumeros: number = 0

  escolas = []

  constructor(
    private _mock: MockService
  ) { }

  ngOnInit(): void {
    this.escolasNumeros = this._mock.getContentForKey<Array<Escola>>('escola').length 
    this.turmasNumeros = this._mock.getContentForKey<Array<Turma>>('turma').length
    this.escolas = this._mock.getContentForKey('escola')
  }

}
