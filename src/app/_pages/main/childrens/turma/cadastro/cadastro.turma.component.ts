import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Escola } from 'src/app/_shared/_extensions/escola.model';
import { MockService } from 'src/app/_services/mock.service';
import { Turma } from 'src/app/_shared/_extensions/turma.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.turma.component.html',
  styleUrls: ['./cadastro.turma.component.css']
})
export class CadastroTurmaComponent implements OnInit {

  _nome: string = '';
  _escolaId: number | string = '';
  _quantiaAlunos: number = 0

  escolas: Array<Escola> = []

  cadastrarForm = new FormGroup({
    nome: new FormControl(this._nome, [Validators.required]),
    escolaId: new FormControl(this._escolaId, [Validators.required]),
    quantiaAlunos: new FormControl(this._quantiaAlunos, [Validators.required]),
  })

  constructor(
    private _mock: MockService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.escolas = this._mock.getContentForKey('escola')
  }

  salvar(formValue) {
    this._mock.setContentForKey('turma', {
      escola: this._mock.getContentForKey<Array<Escola>>('escola').filter(e => {
        return e.id == parseInt(formValue.escolaId)
      })[0],
      escolaId: parseInt(formValue.escolaId),
      nome: formValue.nome,
      quantiaAlunos: formValue.quantiaAlunos
    } as Turma).then( () => {
      this._router.navigate(['/sistema/turmas'])
    })
  }

}
