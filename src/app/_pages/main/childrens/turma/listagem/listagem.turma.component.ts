import { Component, OnInit } from '@angular/core';
import { Turma } from 'src/app/_shared/_extensions/turma.model';
import { MockService } from 'src/app/_services/mock.service';
import { Escola } from 'src/app/_shared/_extensions/escola.model';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.turma.component.html',
  styleUrls: ['./listagem.turma.component.css']
})
export class ListagemTurmaComponent implements OnInit {

  turmas: Array<Turma> = []

  constructor(
    private _mock: MockService
  ) { }

  ngOnInit(): void {
    this.turmas = this._mock.getContentForKey('turma')
    this.turmas.forEach((turma) => {
      turma.escola = this._mock.getContentForKey<Array<Escola>>('escola').filter(e => {
        return e.id == turma.escolaId
      })[0] || {} as Escola
    })
  }

}
