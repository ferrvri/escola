import { Component, OnInit } from '@angular/core';
import { MockService } from 'src/app/_services/mock.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { estadosArray } from 'src/app/_shared/_extensions/estados.array';
import { Router } from '@angular/router';


@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.component.html',
  styleUrls: ['./cadastro.component.css']
})
export class CadastroEscolaComponent implements OnInit {

  _nome: string = '';
  _rua: string = '';
  _numero: string = '';
  _cidade: string = '';
  _estado: string = '';

  estados = estadosArray

  cadastrarForm: FormGroup = new FormGroup({
    nome: new FormControl(this._nome, [Validators.required]),
    rua: new FormControl(this._rua, [Validators.required]),
    numero: new FormControl(this._numero, [Validators.required]),
    cidade: new FormControl(this._cidade, [Validators.required]),
    estado: new FormControl(this._estado, [Validators.required])
  })

  constructor(
    private _mock: MockService,
    private _router: Router
  ) { }

  ngOnInit(): void {
  }

  salvar(formValue){
    this._mock.setContentForKey('escola', formValue).then( () => {
      this._router.navigate(['/sistema/escolas'])
    })
  }

}
