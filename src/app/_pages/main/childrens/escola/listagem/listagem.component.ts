import { Component, OnInit } from '@angular/core';
import { Escola } from 'src/app/_shared/_extensions/escola.model';
import { MockService } from 'src/app/_services/mock.service';

@Component({
  selector: 'app-listagem',
  templateUrl: './listagem.component.html',
  styleUrls: ['./listagem.component.css']
})
export class ListagemEscolaComponent implements OnInit {

  escolas: Array<Escola> = []

  constructor(
    private _mock: MockService
  ) { }

  ngOnInit(): void {
    this.escolas = this._mock.getContentForKey('escola')
  }

}
