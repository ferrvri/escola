import { Component, OnInit } from '@angular/core';
import { MockService } from 'src/app/_services/mock.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(
    private _mock: MockService
  ) { }

  ngOnInit(): void {
    this._mock.init()
  }

}
