import { Escola } from 'src/app/_shared/_extensions/escola.model';
import { Turma } from 'src/app/_shared/_extensions/turma.model';

export interface MockContent{
    escola: Array<Escola>;
    turma: Array<Turma>;
}