import { Injectable, OnInit } from "@angular/core";
import { MockContent } from './_extensions/mock.content';

@Injectable()
export class MockService {

    DATA: MockContent = {
        escola: [],
        turma: []
    }

    constructor() {}

    init(){
        if (!localStorage.getItem('mock')){ 
            localStorage.setItem('mock', JSON.stringify(this.DATA))
        }
    }

    /**
     * 
     * @param key Chave do dicionario para busca de elementos [escola, aluno, turma]
     * 
     */
    getContentForKey<T>(key: string): T{
        return JSON.parse(localStorage.getItem('mock'))[key] || {};
    }

    /**
     * 
     * @param key Chave do dicionario para busca de elementos [escola, aluno, turma]
     * @param content Conteudo para inserção no mock [Object || String]
     */
    setContentForKey(key: string, content: Object | string){
        return new Promise( (resolve, reject) => {
            let _data = JSON.parse(localStorage.getItem('mock'))[key]
            let _ctn = undefined
            if (content instanceof Object) {
                _ctn = content
            }else{
                _ctn = JSON.parse(content)
            }

            let id = 0
            
            if (_data[_data.length -1] !== undefined) id = _data[_data.length -1].id + 1
            else id = 1

            _ctn.id = id
            _data.push(_ctn)

            this.DATA = JSON.parse(localStorage.getItem('mock'))
            this.DATA[key] = _data
            
            localStorage.setItem('mock', JSON.stringify(this.DATA))
            resolve()

            if (!localStorage.getItem('mock')) reject('Mock nao existe')
        })
    }


}