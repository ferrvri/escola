import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './_pages/main/main.component';
import { DashboardComponent } from './_pages/main/childrens/dashboard/dashboard.component';
import { ListagemEscolaComponent } from './_pages/main/childrens/escola/listagem/listagem.component';
import { CadastroEscolaComponent } from './_pages/main/childrens/escola/cadastro/cadastro.component';
import { ListagemTurmaComponent } from './_pages/main/childrens/turma/listagem/listagem.turma.component';
import { CadastroTurmaComponent } from './_pages/main/childrens/turma/cadastro/cadastro.turma.component';

const routes: Routes = [
  {path: '', redirectTo: 'sistema/dashboard', pathMatch: 'full'},
  {path: 'sistema', component: MainComponent, children: [
    {path: 'dashboard', component: DashboardComponent},
    {path: 'escolas', component: ListagemEscolaComponent},
    {path: 'cadastro-escola', component: CadastroEscolaComponent},
    {path: 'turmas', component: ListagemTurmaComponent},
    {path: 'cadastro-turma', component: CadastroTurmaComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
