import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './_pages/main/main.component';
import { DashboardComponent } from './_pages/main/childrens/dashboard/dashboard.component';
import { CadastroEscolaComponent } from './_pages/main/childrens/escola/cadastro/cadastro.component';
import { ListagemEscolaComponent } from './_pages/main/childrens/escola/listagem/listagem.component';
import { MockService } from './_services/mock.service';
import { CadastroTurmaComponent } from './_pages/main/childrens/turma/cadastro/cadastro.turma.component';
import { ListagemTurmaComponent } from './_pages/main/childrens/turma/listagem/listagem.turma.component';
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    DashboardComponent,
    CadastroEscolaComponent,
    ListagemEscolaComponent,
    CadastroTurmaComponent,
    ListagemTurmaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [
    MockService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
